import { Container } from '@mui/material';
import React from 'react';
import styles from './coach.module.scss';
import Image from 'next/image'
import {coaches} from '../../../mock/coaches'
import { motion } from 'framer-motion'

const textAnimation = {
    hidden: {
      x: -100,
      opacity: 0
    },
    visible: custom =>({
      x: 0,
      opacity: 1,
      transition: { delay: custom * 0.2 },
    })
  }
  const blockAnimation = {
    hidden: {
      y: 100,
      opacity: 0
    },
    visible: custom =>({
      y: 0,
      opacity: 1,
      transition: { delay: custom * 0.2 },
    })
  }


function CoachesList(props) {
    return (
        <motion.div initial='hidden' whileInView='visible' className={styles.coaches}>
            <Container>
                <motion.h2 variants={textAnimation} custom={1} className={styles.coaches_title}>Тренеры</motion.h2>
                
                <div  className={styles.coaches_list}>
                {coaches?.map(data => (
                    <motion.div variants={blockAnimation} custom={2} key={data?.id} className={styles.coaches_list_item}>
                        <div className={styles.coaches_img}>
                            <Image
                            src={data?.img}
                            alt=''
                            width={400}
                            height={400}
                            // layout='fill'
                            />
                        </div>
                        <div className={styles.coaches_info}>
                            <h2>{data?.name}</h2>
                            <p className={styles.coaches_status}>{data?.title}</p>
                            <p className={styles.coaches_place}>Город: {data?.place}</p>
                            <p className={styles.coaches_birthDate}>Год рождения: {data?.birthDate}</p>
                        </div>
                    </motion.div>  
                ))}
                </div>
            </Container>
        </motion.div>
    );
}

export default CoachesList;
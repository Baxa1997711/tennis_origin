import React, {useState} from 'react'
import styles from '../video.module.scss'
import { 
    PlayIcon,
    CalendarIcon2,
    CancelIcon,
    PlayIcon2
} from '../../../svg.js'
import Image from 'next/image'
import { Button, Dialog } from '@mui/material'
import VideoImg from '../../../../../public/images/Video_bannerImg.png'
import { makeStyles } from '@mui/styles'

const useStyles = makeStyles({
    
})

function VideoList({ video, title, date }) {
    const classes = useStyles();
    
    const [videoOpen, setVideoOpen] = useState(false)
    
    // Video modal
    const handleOpen = () => setVideoOpen(true)
    const handleClose = () => setVideoOpen(false)
        
    const [showIframe, setShowIframe] = useState(false)
    const Close = () => {
        setVideoOpen(false);  
        setShowIframe(false)
    }
    
    
    return (
        <>
            <div className={styles.videoList}>
            <div className={styles.videoList_item} onClick={handleOpen}>
                <div className={styles.videoList_playIcon}>
                <button onClick={handleOpen} className={styles.videoList_playIconBtn}>
                    <div className={styles.playIcon}>
                    <PlayIcon/>
                    </div>
                </button>
                </div>
                   <Image
                   src={video}
                   alt=''
                   layout='fill'
                   /> 
                <div className={styles.videoList_context}>
                <div className={styles.videoList_date}>
                  <CalendarIcon2/>  
                  <span>{date}</span>
                </div>
                <h4 className={styles.videoList_title}>{title}</h4>
                </div>
            </div>
            
            <Dialog maxWidth onClose={() => Close()} open={videoOpen}
            PaperProps={{
                style: {backgroundColor: "rgba(37, 44, 50, 0.01)", boxShadow: 'none'}}}
            BackdropProps={{ style: {backgroundColor: 'rgba(37, 44, 50, 0.9)'} }}
            id='video_modal'    
            >
                <button onClick={() => Close()} className={styles.videoList_closeIcon}>
                <CancelIcon/>  
                </button>
              {showIframe && showIframe ? (
                 <div className={styles.videoList_video}>
                    <iframe className={styles.iframe_video}  id='iframe_video' autoPlay src="https://www.youtube.com/embed/qux4-yWeZvo?autoplay=1" title='sadasd' frame-border="0" allow="autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allow-fullscreen ></iframe>
                </div> 
              ) : (
                <div className={styles.videoList_layer}>
                <div className={styles.videoList_layer_btn}>
                    <button onClick={() => setShowIframe(true)} className={styles.videoList_btns}>
                        <PlayIcon2 className={styles.playIcon}/>
                    </button>
                </div>
                <div className={styles.videoList_img}>
                    <Image
                    src={VideoImg}
                    alt=''
                    />
                </div>
                <div className={styles.videoList_layer_context}>
                    <p >
                        <CalendarIcon2/>
                        <span>06.09.2022</span>
                    </p>
                    <h4>Чемпионат в Венгрии - Роман Ветров</h4>
                </div>
                </div>
              )}
            </Dialog>
            </div>
            
            {/* Mobile Dialog */}
            <Dialog open={videoOpen} maxWidth onClose={() => Close()} id='mobile_videoList'
            PaperProps={{
                style: {backgroundColor: "rgba(0, 0, 0);", boxShadow: 'none'}}}
            BackdropProps={{ style: {backgroundColor: 'rgba(0, 0, 0,);'} }}
            >
                {showIframe && showIframe ? (
                    <div className={styles.mobile_videoList}>
                    <button onClick={() => Close()} className={styles.mobile_cancel_btn}><CancelIcon/></button>
                      <div className={styles.mobile_video}>
                        <iframe className={styles.mobile_iframe_video}  autoPlay src="https://www.youtube.com/embed/qux4-yWeZvo?autoplay=1" title='sadasd' frame-border="0" allow="autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allow-fullscreen ></iframe>
                      </div>  
                      <div className={styles.mobile_context}>
                        <p >
                            <CalendarIcon2/>
                            <span>06.09.2022</span>
                        </p>
                        <h4>Чемпионат в Венгрии - Роман Ветров</h4>
                      </div>
                    </div>
              ) : (
                <div className={styles.mobile_layer}>
                <button onClick={() => Close()} className={styles.mobile_cancel_btn}><CancelIcon/></button>
                  <div className={styles.mobile_video}>
                    <div className={styles.mobile_img}>
                        <Image
                        src={VideoImg}
                        alt=''
                        width='100%'
                        height='100%'
                        layout='fill'
                        />
                    <div className={styles.mobile_playBtn}><button onClick={() => setShowIframe(true)}><PlayIcon2/></button></div>
                    </div>
                  </div>  
                  <div className={styles.mobile_context}>
                    <p >
                        <CalendarIcon2/>
                        <span>06.09.2022</span>
                    </p>
                    <h4>Чемпионат в Венгрии - Роман Ветров</h4>
                  </div>
                </div>
              )}
                
            </Dialog>
        </>
    );
}

export default VideoList;
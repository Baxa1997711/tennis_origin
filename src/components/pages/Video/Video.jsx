import React from 'react'
import styles from './video.module.scss'
import VideoList from './VideoList/VideoList'
import { video } from '../../../mock/video'
import { Container } from '@mui/material';
import { Button, Dialog } from "@mui/material";
import { motion } from 'framer-motion'
import { useState } from 'react';

const textAnimation = {
    hidden: {
      x: -100,
      opacity: 0
    },
    visible: custom =>({
      x: 0,
      opacity: 1,
      transition: { delay: custom * 0.2 },
    })
  }
  const blockAnimation = {
    hidden: {
      y: 100,
      opacity: 0
    },
    visible: custom =>({
      y: 0,
      opacity: 1,
      transition: { delay: custom * 0.2 },
    })
  }


function Video() {
    const [showVideo, setShowVideo] = useState(false)
    
    // Show content functions
    const showContent = () => setShowVideo(true)
    const hideContent = () => setShowVideo(false)
    
    
    return (
        <motion.div initial='hidden' whileInView='visible' viewport={{ amount: 0.1, once: true}} className={styles.video}>
            <Container >
            <div className={styles.video_content}>
                <motion.h4 variants={textAnimation} custom={1} className={styles.video_content_title}>Видео</motion.h4>
                <div className={styles.video_content_list}>
                    <motion.div variants={blockAnimation} custom={2} className={styles.video_content_item}>
                      {!showVideo ?  (
                        video?.filter((item, index) => index < 12).map(item => (
                          <VideoList key={item?.id} title={item?.title} date={item?.date} video={item?.video}/>
                        ))
                      ) : (
                         video?.map(item => (
                           <VideoList key={item?.id} title={item?.title} date={item?.date} video={item?.video}/>
                         )) 
                      )}
                    </motion.div>
                </div>
                <div className={styles.content_length}>
                {!showVideo ? (
                  <Button className={styles.showContentBtn} onClick={showContent}>Показать больше</Button>
                  ) : (
                    <Button className={styles.showContentBtn} onClick={hideContent}>Показать меньше</Button>
                  )}
                </div>
            </div>
            </Container>
        </motion.div>
    );
}

export default Video;
import React from 'react'
import styles from '../newsItem.module.scss'
import Image from 'next/image'
import { CalendarIcon2 } from '../../../../svg.js'
import { motion } from 'framer-motion'

const textAnimation = {
    hidden: {
        x: -100,
        opacity: 0
    },
    visible: custom =>({
        x: 0,
        opacity: 1,
        transition: { delay: custom * 0.2 }
    })
}



function SingleNews({ data }) {
    
    return (
        <motion.div initial='hidden' whileInView='visible' className={styles.singleNews}>
            <div className={styles.singleNews_content}>
                <motion.h2 variants={textAnimation} custom={1} className={styles.singleNews_content_title}>Конкурс среди воспитателей дошкольных образовательных учреждений</motion.h2>
                <motion.div variants={textAnimation} custom={2} className={styles.singleNews_date}>
                   <CalendarIcon2/>  
                   <span>06.09.2022</span>
                </motion.div>
                <motion.div variants={textAnimation} custom={2} className={styles.singleNews_img}>
                    <Image
                    src={data[0]?.img}
                    alt=''
                    width={823}
                    height={420}
                    />
                </motion.div>
                <div className={styles.singleNews_context}>
                    <p className={styles.single_text}>{data[0].text1}</p>
                    <p className={styles.single_text}>{data[0].text2}</p>
                    <p className={styles.single_text}>{data[0].text3}</p>
                    <p className={styles.single_text}>{data[0].text4}</p>
                    <p className={styles.single_text}>{data[0].text5}</p>
                    <p className={styles.single_text}>{data[0].text6}</p>
                    <p className={styles.single_text}>{data[0].text7}</p>
                    <p className={styles.single_text}>{data[0].text8}</p>
                    <p className={styles.single_text}>{data[0].text9}</p>
                    <p className={styles.single_text}>{data[0].text10}</p>
                    <p className={styles.single_text}>{data[0].text11}</p>
                    <p className={styles.single_text}>{data[0].text12}</p>
                    <p className={styles.single_text}>{data[0].text13}</p>
                    <p className={styles.single_text}>{data[0].text14}</p>
                    <p className={styles.single_text}>{data[0].text15}</p>
                    <p className={styles.single_text}>{data[0].text16}</p>
                    <p className={styles.single_text}>{data[0].text17}</p>
                </div>
            </div>
        </motion.div>
    );
}

export default SingleNews;

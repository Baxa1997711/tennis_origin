import React from 'react'
import styles from './newsItem.module.scss'
import NewsSidebar from './NewsSidebar/NewsSidebar';
import SingleNews from './SingleNews/SingleNews'
import { data } from './data';
import { useRouter } from 'next/router'
import { Container } from '@mui/material';



function NewsItem() {
    
    return (
        <div className={styles.newsItem}>
           <Container>
                <div className={styles.newsItem_content_item}>
                    <SingleNews data={data}/>
                    <NewsSidebar/>
                </div>
           </Container>
        </div>
    );
}

export default NewsItem;
import React from 'react'
import styles from '../newsItem.module.scss'
import { news } from '../../../../../mock/news';
import { motion } from 'framer-motion'
import NewsBlockItem from '../../../../UI/NewsBlockItem/NewsBlockItem'

const blockAnimation = {
    hidden: {
        x: 100,
        opacity: 0
    },
    visible: custom => ({
        x: 0,
        opacity: 1,
        transiton: {delay: custom * 0.4}
    })
}


function NewsSidebar(props) {
  const newsItem = news?.slice(0, 4);
  
    return (
        <motion.div initial='hidden' whileInView='visible' viewport={{amount: 0.1, once: true}} className={styles.newsSidebar}>
            <div variants={blockAnimation} custom={1} className={styles.newsSidebar_content}>
               {news?.filter((news, index) => index < 4).map(item => (
                <NewsBlockItem 
                key={item?.id}
                title={item?.title}
                handleClick={() => handleBranch(index)}
                subtitle={item?.subtitle}
                date={item?.date}
                img={item?.img}
                /> 
               ))}
               
               
            </div>
        </motion.div>
    );
}

export default NewsSidebar;
export const data = [
    {
        id: 'news',
        title: 'Конкурс среди воспитателей Колледжей ',
        date: '06.09.2022',
        img: '/images/singlepage_img.png',
        text1: 'В результате особого внимания, уделяемого развитию физической культуры и спорта под руководством Президента Ислама Каримова, молодежь нашей страны подрастает талантливым и смелым поколением.',
        text2: 'Министерство развития спорта Узбекистана, Национальный олимпийский комитет, спортивные федерации и ассоциации нашей страны делают все возможное, чтобы создать эти возможности для наших юных спортсменов.',
        text3: 'В частности, в последние годы Федерация настольного тенниса Узбекистана проводит работу по повышению квалификации юных спортсменов, популяризации вида спорта среди молодежи страны и отбору из их числа талантливой молодежи. Кроме того, в целях дальнейшего повышения квалификации спортсменов в составе сборной федерация направляет спортсменов в клубы за рубежом.',
        text4: 'Воспользовавшись случаем, Абдулазиз Анарбаев, который в настоящее время тренируется в клубе SV Arminia Hannover, защищая честь немецкого города Ганновер, рассказал о будущем спорта и своей деятельности в Германии.',
        text5: '-Абдулазиз, со скольки лет ты играешь в настольный теннис и каковы твои достижения в национальных соревнованиях?',
        text6: '-Я занимаюсь этим видом спорта с 11 лет. Мой отец еще в детстве заметил, что я увлекаюсь этим видом спорта, и отвел меня в кружок тренера Обида Гафурова. Я начал изучать ряд секретов настольного тенниса от своего первого тренера. Я участвовал в районных, городских и областных соревнованиях, участвовал в чемпионатах страны и стал чемпионом страны среди молодежи 2000 года рождения. Я также завоевал бронзовую медаль на чемпионатах Узбекистана 2020 и 2021 годов среди взрослых.',
        text6: '- О вашей деятельности в Германии? Почему ты учишься в Германии?',
        text7: 'В настоящее время я работаю в SV Arminia Hannover в Ганновере. Прежде всего, главная цель моего приезда сюда – совершенствовать свой стиль и мастерство, участвовать в различных престижных спортивных соревнованиях, чемпионатах Азии и мира и Олимпийских играх, еще больше приумножать честь нашей страны.',
        text8: 'За свою карьеру я практиковал в Латвии, Молдове, Турции и Казахстане. Однако я чувствовал, что обучение в Германии было другим. Я в Ганновере с 2019 года, и условия здесь отличные. Очки, оборудование для настольного тенниса, а также финансовые возможности высоки. Также цель моего опыта здесь — получить билет на Олимпийские игры 2024 года в Париже.',
        text9: 'Какие условия для вас создает узбекский настольный теннис?',
        text10: '- Федерация настольного тенниса нашей страны всегда старается создавать возможности для наших спортсменов и у нее это получается. Особенно в последние годы были предоставлены новые залы, удобное и современное оборудование для настольного тенниса. Также создаются все возможности для юных спортсменов выезжать на международные соревнования и набираться опыта. Мы благодарны нашей федерации за это.',
        text11: '- Чем вы объясните провал чемпионата Азии в Катаре в 2021 году?',
        text12: '- Если говорить о чемпионате континента в Дохе, то я лично выиграл первый чемпионат Азии. Мы успешно выступили в групповом этапе соревнований и победили всех соперников. Наша команда была сильной. К сожалению, в следующем туре мы проиграли Саудовской Аравии со счетом 3:2. У нас не было опыта участия в соревнованиях, и мы не могли играть в ту игру, которую хотели. Тем не менее, это был отличный урок для нас работать над собой. Надеюсь, что на следующих соревнованиях мы покажем высокие результаты, сделав правильные выводы из своих ошибок и недочетов.',
        text13: '- О планах на будущее?',
        text14: '- В эти дни я продолжаю тренировки. Что касается наших планов, то я хочу участвовать в международных соревнованиях и получить больше игровой практики. И я хочу принять достойное участие в соревнованиях, в которых участвует сборная нашей страны.',
        text15: '- Спасибо за содержательную беседу!',
        text16: '- Я хотел бы поблагодарить все СМИ.',
        text17: 'Да, пользуясь широким спектром возможностей, созданных сегодня в нашей стране, для достижения поставленных целей, для доказательства всему миру, что такой мощный дуэт, как Узбекистан, является достойным фапзандлапи, мы гордимся высокими результатами, достигнутыми нашим споптчилапом.'
    },
    {
        id: '0',
        title: 'Конкурс среди воспитателей ',
        date: '06.09.2022',
        img: '/images/image 2 (3).png',
        text1: 'В результате особого внимания, уделяемого развитию физической культуры и спорта под руководством Президента Ислама Каримова, молодежь нашей страны подрастает талантливым и смелым поколением.',
        text2: 'Министерство развития спорта Узбекистана, Национальный олимпийский комитет, спортивные федерации и ассоциации нашей страны делают все возможное, чтобы создать эти возможности для наших юных спортсменов.',
        text3: 'В частности, в последние годы Федерация настольного тенниса Узбекистана проводит работу по повышению квалификации юных спортсменов, популяризации вида спорта среди молодежи страны и отбору из их числа талантливой молодежи. Кроме того, в целях дальнейшего повышения квалификации спортсменов в составе сборной федерация направляет спортсменов в клубы за рубежом.',
        text4: 'Воспользовавшись случаем, Абдулазиз Анарбаев, который в настоящее время тренируется в клубе SV Arminia Hannover, защищая честь немецкого города Ганновер, рассказал о будущем спорта и своей деятельности в Германии.',
        text5: '',
        text6: '',
        text6: '',
        text7: '',
        text8: 'За свою карьеру я практиковал в Латвии, Молдове, Турции и Казахстане. Однако я чувствовал, что обучение в Германии было другим. Я в Ганновере с 2019 года, и условия здесь отличные. Очки, оборудование для настольного тенниса, а также финансовые возможности высоки. Также цель моего опыта здесь — получить билет на Олимпийские игры 2024 года в Париже.',
        text9: 'Какие условия для вас создает узбекский настольный теннис?',
        text10: '- Федерация настольного тенниса нашей страны всегда старается создавать возможности для наших спортсменов и у нее это получается. Особенно в последние годы были предоставлены новые залы, удобное и современное оборудование для настольного тенниса. Также создаются все возможности для юных спортсменов выезжать на международные соревнования и набираться опыта. Мы благодарны нашей федерации за это.',
        text11: '- Чем вы объясните провал чемпионата Азии в Катаре в 2021 году?',
        text12: '- Если говорить о чемпионате континента в Дохе, то я лично выиграл первый чемпионат Азии. Мы успешно выступили в групповом этапе соревнований и победили всех соперников. Наша команда была сильной. К сожалению, в следующем туре мы проиграли Саудовской Аравии со счетом 3:2. У нас не было опыта участия в соревнованиях, и мы не могли играть в ту игру, которую хотели. Тем не менее, это был отличный урок для нас работать над собой. Надеюсь, что на следующих соревнованиях мы покажем высокие результаты, сделав правильные выводы из своих ошибок и недочетов.',
        text13: '',
        text14: '',
        text15: '- Спасибо за содержательную беседу!',
        text16: '- Я хотел бы поблагодарить все СМИ.',
        text17: 'Да, пользуясь широким спектром возможностей, созданных сегодня в нашей стране, для достижения поставленных целей, для доказательства всему миру, что такой мощный дуэт, как Узбекистан, является достойным фапзандлапи, мы гордимся высокими результатами, достигнутыми нашим споптчилапом.'
    },
    {
        id: '1',
        title: 'Соревнование среди воспитателей дошкольных образовательных учреждений ',
        date: '06.09.2022',
        img: '/images/image 2 (3).png',
        text1: 'В результате особого внимания, уделяемого развитию физической культуры и спорта под руководством Президента Ислама Каримова, молодежь нашей страны подрастает талантливым и смелым поколением.',
        text2: '',
        text3: '',
        text4: 'Воспользовавшись случаем, Абдулазиз Анарбаев, который в настоящее время тренируется в клубе SV Arminia Hannover, защищая честь немецкого города Ганновер, рассказал о будущем спорта и своей деятельности в Германии.',
        text5: '',
        text6: '',
        text6: '- О вашей деятельности в Германии? Почему ты учишься в Германии?',
        text7: 'В настоящее время я работаю в SV Arminia Hannover в Ганновере. Прежде всего, главная цель моего приезда сюда – совершенствовать свой стиль и мастерство, участвовать в различных престижных спортивных соревнованиях, чемпионатах Азии и мира и Олимпийских играх, еще больше приумножать честь нашей страны.',
        text8: 'За свою карьеру я практиковал в Латвии, Молдове, Турции и Казахстане. Однако я чувствовал, что обучение в Германии было другим. Я в Ганновере с 2019 года, и условия здесь отличные. Очки, оборудование для настольного тенниса, а также финансовые возможности высоки. Также цель моего опыта здесь — получить билет на Олимпийские игры 2024 года в Париже.',
        text9: '',
        text10: '',
        text11: '- Чем вы объясните провал чемпионата Азии в Катаре в 2021 году?',
        text12: '- Если говорить о чемпионате континента в Дохе, то я лично выиграл первый чемпионат Азии. Мы успешно выступили в групповом этапе соревнований и победили всех соперников. Наша команда была сильной. К сожалению, в следующем туре мы проиграли Саудовской Аравии со счетом 3:2. У нас не было опыта участия в соревнованиях, и мы не могли играть в ту игру, которую хотели. Тем не менее, это был отличный урок для нас работать над собой. Надеюсь, что на следующих соревнованиях мы покажем высокие результаты, сделав правильные выводы из своих ошибок и недочетов.',
        text13: '- О планах на будущее?',
        text14: '- В эти дни я продолжаю тренировки. Что касается наших планов, то я хочу участвовать в международных соревнованиях и получить больше игровой практики. И я хочу принять достойное участие в соревнованиях, в которых участвует сборная нашей страны.',
        text15: '- Спасибо за содержательную беседу!',
        text16: '- Я хотел бы поблагодарить все СМИ.',
        text17: 'Да, пользуясь широким спектром возможностей, созданных сегодня в нашей стране, для достижения поставленных целей, для доказательства всему миру, что такой мощный дуэт, как Узбекистан, является достойным фапзандлапи, мы гордимся высокими результатами, достигнутыми нашим споптчилапом.'
    },
    {
        id: '2',
        title: 'Соревнование среди молодежей страны',
        date: '06.09.2022',
        img: '/images/image 2 (3).png',
        text1: 'В результате особого внимания, уделяемого развитию физической культуры и спорта под руководством Президента Ислама Каримова, молодежь нашей страны подрастает талантливым и смелым поколением.',
        text2: 'Министерство развития спорта Узбекистана, Национальный олимпийский комитет, спортивные федерации и ассоциации нашей страны делают все возможное, чтобы создать эти возможности для наших юных спортсменов.',
        text3: 'В частности, в последние годы Федерация настольного тенниса Узбекистана проводит работу по повышению квалификации юных спортсменов, популяризации вида спорта среди молодежи страны и отбору из их числа талантливой молодежи. Кроме того, в целях дальнейшего повышения квалификации спортсменов в составе сборной федерация направляет спортсменов в клубы за рубежом.',
        text4: 'Воспользовавшись случаем, Абдулазиз Анарбаев, который в настоящее время тренируется в клубе SV Arminia Hannover, защищая честь немецкого города Ганновер, рассказал о будущем спорта и своей деятельности в Германии.',
        text5: '-Абдулазиз, со скольки лет ты играешь в настольный теннис и каковы твои достижения в национальных соревнованиях?',
        text6: '-Я занимаюсь этим видом спорта с 11 лет. Мой отец еще в детстве заметил, что я увлекаюсь этим видом спорта, и отвел меня в кружок тренера Обида Гафурова. Я начал изучать ряд секретов настольного тенниса от своего первого тренера. Я участвовал в районных, городских и областных соревнованиях, участвовал в чемпионатах страны и стал чемпионом страны среди молодежи 2000 года рождения. Я также завоевал бронзовую медаль на чемпионатах Узбекистана 2020 и 2021 годов среди взрослых.',
        text6: '',
        text7: '',
        text8: 'За свою карьеру я практиковал в Латвии, Молдове, Турции и Казахстане. Однако я чувствовал, что обучение в Германии было другим. Я в Ганновере с 2019 года, и условия здесь отличные. Очки, оборудование для настольного тенниса, а также финансовые возможности высоки. Также цель моего опыта здесь — получить билет на Олимпийские игры 2024 года в Париже.',
        text9: '',
        text10: '',
        text11: '- Чем вы объясните провал чемпионата Азии в Катаре в 2021 году?',
        text12: '- Если говорить о чемпионате континента в Дохе, то я лично выиграл первый чемпионат Азии. Мы успешно выступили в групповом этапе соревнований и победили всех соперников. Наша команда была сильной. К сожалению, в следующем туре мы проиграли Саудовской Аравии со счетом 3:2. У нас не было опыта участия в соревнованиях, и мы не могли играть в ту игру, которую хотели. Тем не менее, это был отличный урок для нас работать над собой. Надеюсь, что на следующих соревнованиях мы покажем высокие результаты, сделав правильные выводы из своих ошибок и недочетов.',
        text13: '',
        text14: '',
        text15: '',
        text16: '',
        text17: ''
    },
]
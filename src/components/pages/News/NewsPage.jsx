import React from 'react';
import styles from './newspage.module.scss'
import NewsPageBanner from './NewsPageBanner/NewsPageBanner'
import NewsBlockItem from '../../UI/NewsBlockItem/NewsBlockItem'
import { Container, Pagination, Stack } from '@mui/material'
import { news } from '../../../mock/news'




function NewsPage() {
    return (
        <div className={styles.newspage} id='newspage'>
            <Container>
                <div className={styles.newspage_content}>
                <NewsPageBanner news={news}/>
                <div className={styles.newspage_content_item}>
                { news?.map((newslist) => (
                    <NewsBlockItem key={newslist?.id} title={newslist?.title} subtitle={newslist?.subtitle} date={newslist?.date} img={newslist?.img}/>
                )) }
                </div>
                <div className={styles.pagination}>
                    <Stack spacing='2' >
                        <Pagination  count={5} shape='rounded'/>
                    </Stack>
                </div>
            </div>
            </Container>
        </div>
    );
}

export default NewsPage;
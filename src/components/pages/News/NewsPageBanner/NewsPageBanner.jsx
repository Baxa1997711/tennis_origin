import React, { useState } from 'react';
import styles from '../newspage.module.scss'
import Image from 'next/image'
import Link from 'next/link'
import { Button } from '@mui/material'
import { motion } from 'framer-motion'
import Slider from 'react-slick';
import { ArrowRight, ArrowLeft } from '../../../UI/SlickCarouselArrows/Arrows'
import { news } from '../../../../mock/news'


const data = [
  {
    id: 0,
    img: '/images/newspage_banner1.png',
    title: 'Конкурс среди воспитателей дошкольных',
    subtitle: 'В результате особого внимания, уделяемого развитию физической культуры и спорта под руководством Президента Ислама Каримова, молодежь нашей страны подрастает талантливым и смелым поколением. Министерство развития спорта Узбекистана, Национальный олимпийский комитет...'
  },
  {
    id: 1,
    img: '/images/newspage_banner1.png',
    title: 'Конкурс среди воспитателей дошкольных образовательных учреждений',
    subtitle: 'В результате особого внимания, уделяемого развитию физической культуры и спорта под руководством Президента Ислама Каримова, молодежь нашей страны подрастает талантливым и смелым поколением. Министерство развития спорта Узбекистана, Национальный олимпийский комитет...'
  },
  {
    id: 2,
    img: '/images/newspage_banner1.png',
    title: 'Конкурс',
    subtitle: 'В результате особого внимания, уделяемого развитию физической культуры и спорта под руководством Президента Ислама Каримова, молодежь нашей страны подрастает талантливым и смелым поколением. Министерство развития спорта Узбекистана, Национальный олимпийский комитет...'
  },
]

const textAnimation = {
    hidden: {
      x: 100,
      opacity: 0
    },
    visible: custom =>({
      x: 0,
      opacity: 1,
      transition: { delay: custom * 0.2 },
    })
  }
const imgAnimation = {
    hidden: {
      x: -100,
      opacity: 0
    },
    visible: custom =>({
      x: 0,
      opacity: 1,
      transition: { delay: custom * 0.2 },
    })
  }

function NewsPageBanner() {

  
  const settings = {
    dots: false,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    nextArrow: <ArrowRight styles={styles.arrow_right}/>,
    prevArrow: <ArrowLeft styles={styles.arrow_left}/>,
    swipeToSlide: true
  }
  
    return (
      <Slider {...settings} className={styles.newspage_banner_slider}>
        {news?.filter((item, index) => index < 4).map(item => (
          <div key={item?.id} initial='hidden' whileInView='visible' viewport={{ amoun: 0.1, once: true}} className={styles.newspage_banner}>
          <div className={styles.newspage_banner_content}>
            <div variants={imgAnimation} custom={1} className={styles.newspage_banner_img}>
               <Image
               src={item?.img}
               alt=''
               width={536}
               height={471}
               /> 
            </div>
            <motion.div variants={textAnimation} custom={2} className={styles.newspage_banner_context}>
              <h4 className={styles.newspage_banner_title}>{item?.title}</h4>
              <p className={styles.newspage_banner_subtitle}>{item?.subtitle}</p>
              <Link href={`/news/news`}>
              <a >
              <Button className={styles.newspage_banner_button}>
              Читать подробно
              </Button>
              </a>
              </Link>
            </motion.div>
          </div>
      </div>
        ))}
        </Slider>
    );
}

export default NewsPageBanner;
import React, { useState } from 'react'
import styles from './gallery.module.scss'
import GalleryList from './GalleryList/GalleryList';
import { gallery } from '../../../mock/gallery'
import { Container } from '@mui/material';
import { Button, Dialog } from '@mui/material';
import { motion } from 'framer-motion'

const textAnimation = {
    hidden: {
      y: -100,
      opacity: 0
    },
    visible: custom =>({
      y: 0,
      opacity: 1,
      transition: { delay: custom * 0.2},
    })
  }
  const blockAnimation = {
    hidden: {
      y: 100,
      opacity: 0
    },
    visible: custom =>({
      y: 0,
      opacity: 1,
      transition: { delay: custom * 0.2 },
    })
  }


function Gallery(props) {
  
  const [galleryContent, setGalleryContent] = useState(false);
  
  // Content Length
  const showContent = () => setGalleryContent(true)
  const hideContent = () => setGalleryContent(false)
  
  
    return (
        <motion.div initial='hidden' whileInView='visible' viewport={{ amount: 0.1, once: true }} className={styles.gallery}>
            <Container>
            <div className={styles.gallery_content}>
                <motion.h2 variants={textAnimation} custom={1} className={styles.gallery_content_title}>Галерея</motion.h2>
                
                <div className={styles.gallery_content_list}>
                       <motion.div variants={blockAnimation} custom={2} className={styles.gallery_content_item}>
                        {!galleryContent ? (
                          gallery?.filter((item, index) => index < 10).map(gallerylist => (
                            <GalleryList key={gallerylist?.id} img={gallerylist?.img} title={gallerylist?.title} date={gallerylist?.date} status={gallerylist?.status}/> 
                          ))   ) : (
                              gallery?.map((gallerylist) => (
                              <GalleryList key={gallerylist?.id} img={gallerylist?.img} title={gallerylist?.title} date={gallerylist?.date} status={gallerylist?.status}/> 
                               )) 
                          )}
                       </motion.div>
                </div>
                
                <div className={styles.content_length}>
                {!galleryContent ? (
                  <Button className={styles.showContentBtn} onClick={showContent}>Показать больше</Button>
                  ) : ( <Button className={styles.showContentBtn} onClick={hideContent}>Показать меньше</Button> )
                  }
                
                </div>
                
            </div>
            </Container>
        </motion.div>
    );
}

export default Gallery;
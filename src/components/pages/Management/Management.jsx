import React from 'react';
import styles from './manage.module.scss';
import Image from 'next/image'
import { management } from '../../../mock/management'
import sponsorImg1 from '../../../../public/images/sotrud_img1.png'
import { Container } from '@mui/material';
import { motion } from 'framer-motion'

const textAnimation = {
    hidden: {
      x: -100,
      opacity: 0
    },
    visible: custom =>({
      x: 0,
      opacity: 1,
      transition: { delay: custom * 0.2 },
    })
  }
const blockAnimation = {
    hidden: {
      y: -100,
      opacity: 0
    },
    visible: custom =>({
      y: 0,
      opacity: 1,
      transition: { delay: custom * 0.2 },
    })
  }


function Management(props) {
    
    return (
        <motion.div initial='hidden' whileInView='visible' className={styles.management}>
            <Container>
            <div className={styles.management_content}>
                <motion.h2 variants={textAnimation} custom={1} className={styles.management_content_title}>Руководство Федерации</motion.h2>
                <div className={styles.management_info}>
                {management?.map(item => (
                  <motion.div variants={blockAnimation} custom={2} key={item?.id} className={styles.management_content_item}>
                    <div className={styles.management_content_img}>
                        <Image
                        src={item?.img}
                        alt=''
                        width={400}
                        height={400}
                        />
                    </div>
                    <div className={styles.management_context}>
                        <h4>{item?.name}</h4>
                        <p>{item?.title}</p>
                    </div>
                 </motion.div>  
                ))}
                </div>
                <div className={styles.management_partners}>
                    <div className={styles.management_partners_item}>
                        <h4>Сотрудники:</h4>
                        <div className={styles.management_partners_img}>
                            <Image
                            src={sponsorImg1}
                            alt=''
                            width={205}
                            height={63}
                            />
                        </div>
                    </div>
                    <div className={styles.management_partners_item}>
                        <h4>Информационная поддержка:</h4>
                        <div className={styles.management_partners_img}>
                            <Image
                            src={sponsorImg1}
                            alt=''
                            width={205}
                            height={63}
                            />
                        </div>
                    </div>
                    <div className={styles.management_partners_item}>
                        <h4>Официальные партнёры:</h4>
                        <div className={styles.management_partners_img}>
                            <Image
                            src={sponsorImg1}
                            alt=''
                            width={205}
                            height={63}
                            />
                        </div>
                    </div>
                    <div className={styles.management_partners_item}>
                        <h4>Генеральный партнёр:</h4>
                        <div className={styles.management_partners_img}>
                            <Image
                            src={sponsorImg1}
                            alt=''
                            width={140}
                            height={63}
                            />
                        </div>
                    </div>
                </div>
            </div>
            </Container>
        </motion.div>
    );
}

export default Management;
export const CalendarIcon = () => (
    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M15.8333 3.33337H4.16667C3.24619 3.33337 2.5 4.07957 2.5 5.00004V16.6667C2.5 17.5872 3.24619 18.3334 4.16667 18.3334H15.8333C16.7538 18.3334 17.5 17.5872 17.5 16.6667V5.00004C17.5 4.07957 16.7538 3.33337 15.8333 3.33337Z" stroke="#020A22" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"/>
    <path d="M13.3333 1.66663V4.99996" stroke="#020A22" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"/>
    <path d="M6.66675 1.66663V4.99996" stroke="#020A22" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"/>
    <path d="M2.5 8.33337H17.5" stroke="#020A22" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"/>
</svg>
)
export const CalendarIcon2 = () => (
    <svg width="16" height="17" viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
    <g clipPath="url(#clip0_200_298)">
    <path d="M12.6667 3.33617H3.33333C2.59695 3.33617 2 3.93312 2 4.6695V14.0028C2 14.7392 2.59695 15.3362 3.33333 15.3362H12.6667C13.403 15.3362 14 14.7392 14 14.0028V4.6695C14 3.93312 13.403 3.33617 12.6667 3.33617Z" stroke="#939393" strokeWidth="1.33333" strokeLinecap="round" strokeLinejoin="round"/>
    <path d="M10.6667 2.00282V4.66949" stroke="#939393" strokeWidth="1.33333" strokeLinecap="round" strokeLinejoin="round"/>
    <path d="M5.33325 2.00282V4.66949" stroke="#939393" strokeWidth="1.33333" strokeLinecap="round" strokeLinejoin="round"/>
    <path d="M2 7.33617H14" stroke="#939393" strokeWidth="1.33333" strokeLinecap="round" strokeLinejoin="round"/>
    </g>
    <defs>
    <clipPath id="clip0_200_298">
    <rect width="16" height="16" fill="white" transform="translate(0 0.669495)"/>
    </clipPath>
    </defs>
    </svg>

)

export const DropDownIcon = () => (
    <svg width="12" height="8" viewBox="0 0 12 8" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M11 1.5L6 6.5L1 1.5" stroke="#020A22" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"/>
    </svg>

)
export const LeftArrowIcon = () => (
    <svg width="8" height="14" viewBox="0 0 8 14" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M7 13L1 7L7 1" stroke="white" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
    </svg>

)
export const RightArrowIcon = () => (
    <svg width="8" height="14" viewBox="0 0 8 14" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M1 13L7 7L1 1" stroke="white" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
    </svg>
)

export const InfoArrowIcon = () => (
    <svg width="6" height="11" viewBox="0 0 6 11" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M1 9.5L5 5.5L1 1.5" stroke="#0071FF" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"/>
    </svg>
)

export const InstagramIcon = () => (
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M18.6133 0H5.38715C2.41204 0 0.000244141 2.4118 0.000244141 5.38691V18.6131C0.000244141 21.5882 2.41204 24 5.38715 24H18.6133C21.5884 24 24.0002 21.5882 24.0002 18.6131V5.38691C24.0002 2.4118 21.5884 0 18.6133 0Z" fill="white"/>
    <path d="M8.31333 4.15662H15.7951C15.9614 4.23975 16.2108 4.23975 16.3771 4.32288C17.1067 4.44112 17.7888 4.7605 18.3469 5.24512C18.9049 5.72974 19.3167 6.3604 19.5361 7.06621C19.6588 7.47295 19.7423 7.89051 19.7854 8.31318V15.6287C19.6666 16.5559 19.3243 17.4403 18.7879 18.2058C18.3277 18.7389 17.7508 19.1588 17.102 19.4327C16.4532 19.7067 15.75 19.8273 15.047 19.7853H8.81212C7.69945 19.8393 6.60544 19.4845 5.73626 18.7877C5.20772 18.3311 4.79061 17.7596 4.51685 17.1169C4.24309 16.4743 4.11992 15.7776 4.15677 15.0801V9.26088C4.14304 8.84349 4.17088 8.42578 4.2399 8.01391C4.32385 7.16164 4.67427 6.3575 5.24137 5.71578C5.80848 5.07405 6.56341 4.62739 7.39889 4.43926L8.31333 4.15662ZM5.73626 12.0541V15.2962C5.70082 15.7091 5.75605 16.1247 5.89809 16.514C6.04014 16.9032 6.26555 17.2568 6.55857 17.5498C6.85158 17.8428 7.20509 18.0682 7.59437 18.2102C7.98364 18.3523 8.39926 18.4075 8.81212 18.3721C10.9735 18.4552 13.1349 18.4552 15.2964 18.3721C17.2084 18.3721 18.2891 17.2082 18.3722 15.2962C18.4553 13.1348 18.4553 10.9734 18.3722 8.81197C18.3938 8.40237 18.3289 7.99281 18.182 7.60988C18.035 7.22694 17.8092 6.87917 17.5192 6.58914C17.2292 6.29911 16.8814 6.07329 16.4985 5.92634C16.1155 5.77938 15.706 5.71457 15.2964 5.73611C13.1349 5.65298 10.9735 5.65298 8.81212 5.73611C8.39926 5.70067 7.98364 5.7559 7.59437 5.89794C7.20509 6.03998 6.85158 6.2654 6.55857 6.55841C6.26555 6.85142 6.04014 7.20494 5.89809 7.59422C5.75605 7.98349 5.70082 8.39911 5.73626 8.81197V12.0541Z" fill="#1F1F1F"/>
    <path d="M8.01404 12.0873C8.01623 11.0077 8.4461 9.97286 9.20955 9.20942C9.97299 8.44598 11.0078 8.01611 12.0875 8.01392C13.1612 8.03518 14.1849 8.47117 14.9443 9.23053C15.7036 9.98989 16.1396 11.0137 16.1609 12.0873C16.1587 13.167 15.7288 14.2018 14.9654 14.9653C14.202 15.7287 13.1671 16.1586 12.0875 16.1608C11.0138 16.1395 9.99001 15.7035 9.23065 14.9442C8.4713 14.1848 8.03531 13.161 8.01404 12.0873ZM12.0875 9.51028C11.5825 9.50207 11.0867 9.64482 10.6634 9.92023C10.2401 10.1956 9.90874 10.5912 9.71171 11.0561C9.51467 11.5211 9.46096 12.0343 9.55746 12.53C9.65397 13.0257 9.89629 13.4812 10.2534 13.8383C10.6104 14.1954 11.066 14.4377 11.5617 14.5342C12.0574 14.6307 12.5706 14.577 13.0356 14.38C13.5005 14.1829 13.896 13.8516 14.1715 13.4283C14.4469 13.005 14.5896 12.5091 14.5814 12.0042C14.5814 11.3428 14.3187 10.7084 13.851 10.2407C13.3832 9.77303 12.7489 9.51028 12.0875 9.51028Z" fill="#1F1F1F"/>
    <path d="M15.2464 7.84766C15.2418 7.7154 15.2644 7.58361 15.3129 7.46047C15.3614 7.33734 15.4347 7.22551 15.5283 7.13193C15.6219 7.03836 15.7337 6.96504 15.8568 6.91655C15.98 6.86806 16.1118 6.84544 16.244 6.85008C16.5028 6.86759 16.7465 6.97831 16.9299 7.16173C17.1134 7.34516 17.2241 7.58885 17.2416 7.84766C17.2416 8.11223 17.1365 8.36597 16.9494 8.55305C16.7623 8.74013 16.5086 8.84523 16.244 8.84523C15.9794 8.84523 15.7257 8.74013 15.5386 8.55305C15.3515 8.36597 15.2464 8.11223 15.2464 7.84766Z" fill="#1F1F1F"/>
    </svg>

)
export const FacebookIcon = () => (
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
    <g clipPath="url(#clip0_200_746)">
    <path d="M24.0002 12C24.0002 5.37258 18.6277 0 12.0002 0C5.37282 0 0.000244141 5.37258 0.000244141 12C0.000244141 17.9895 4.38845 22.954 10.1252 23.8542V15.4688H7.07837V12H10.1252V9.35625C10.1252 6.34875 11.9168 4.6875 14.6578 4.6875C15.9703 4.6875 17.344 4.92188 17.344 4.92188V7.875H15.8309C14.3402 7.875 13.8752 8.80008 13.8752 9.75V12H17.2034L16.6713 15.4688H13.8752V23.8542C19.612 22.954 24.0002 17.9895 24.0002 12Z" fill="white"/>
    <path d="M16.6713 15.4688L17.2034 12H13.8752V9.75C13.8752 8.80102 14.3402 7.875 15.8309 7.875H17.344V4.92188C17.344 4.92188 15.9708 4.6875 14.6578 4.6875C11.9168 4.6875 10.1252 6.34875 10.1252 9.35625V12H7.07837V15.4688H10.1252V23.8542C11.3677 24.0486 12.6328 24.0486 13.8752 23.8542V15.4688H16.6713Z" fill="#1F1F1F"/>
    </g>
    <defs>
    <clipPath id="clip0_200_746">
    <rect width="24" height="24" fill="white" transform="translate(0.000244141)"/>
    </clipPath>
    </defs>
    </svg>
    
)
export const TelegramIcon = () => (
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
    <g clipPath="url(#clip0_200_749)">
    <path fillRule="evenodd" clipRule="evenodd" d="M24.0002 12C24.0002 18.6274 18.6277 24 12.0002 24C5.37283 24 0.000244141 18.6274 0.000244141 12C0.000244141 5.37258 5.37283 0 12.0002 0C18.6277 0 24.0002 5.37258 24.0002 12ZM12.4303 8.85893C11.2631 9.3444 8.93038 10.3492 5.43214 11.8733C4.86408 12.0992 4.5665 12.3202 4.53941 12.5363C4.49363 12.9015 4.95096 13.0453 5.57372 13.2411C5.65843 13.2678 5.7462 13.2954 5.83618 13.3246C6.44888 13.5238 7.27307 13.7568 7.70153 13.766C8.09019 13.7744 8.52397 13.6142 9.00288 13.2853C12.2714 11.079 13.9586 9.96381 14.0646 9.93977C14.1393 9.92281 14.2428 9.90148 14.313 9.96385C14.3832 10.0262 14.3763 10.1443 14.3688 10.176C14.3235 10.3691 12.5284 12.0381 11.5994 12.9018C11.3098 13.171 11.1043 13.362 11.0623 13.4056C10.9682 13.5033 10.8724 13.5958 10.7802 13.6846C10.211 14.2333 9.78415 14.6448 10.8039 15.3168C11.2939 15.6397 11.686 15.9067 12.0772 16.1731C12.5044 16.4641 12.9305 16.7543 13.4819 17.1157C13.6223 17.2077 13.7565 17.3034 13.8871 17.3965C14.3843 17.751 14.831 18.0694 15.3828 18.0186C15.7035 17.9891 16.0347 17.6876 16.2029 16.7884C16.6005 14.6631 17.3819 10.0585 17.5625 8.16097C17.5783 7.99473 17.5584 7.78197 17.5424 7.68857C17.5264 7.59518 17.493 7.46211 17.3716 7.3636C17.2278 7.24694 17.0059 7.22234 16.9066 7.22408C16.4553 7.23203 15.7628 7.47282 12.4303 8.85893Z" fill="white"/>
    </g>
    <defs>
    <clipPath id="clip0_200_749">
    <rect width="24" height="24" fill="white" transform="translate(0.000244141)"/>
    </clipPath>
    </defs>
    </svg>

)

export const PlayIcon = () => (
    <svg width="16" height="18" viewBox="0 0 16 18" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M14.9811 7.92473C15.8311 8.41547 15.8311 9.64234 14.9811 10.1331L2.5498 17.3103C1.6998 17.801 0.637303 17.1876 0.637304 16.2061L0.637304 1.85172C0.637304 0.870223 1.6998 0.25679 2.5498 0.747538L14.9811 7.92473Z" fill="black"/>
    </svg>
)
export const PlayIcon2 = () => (
    <svg width="25" height="27" viewBox="0 0 25 27" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M23.0936 11.5669C24.4058 12.3246 24.4059 14.2187 23.0936 14.9763L3.90147 26.0569C2.58919 26.8145 0.948834 25.8675 0.948834 24.3522L0.948835 2.19105C0.948835 0.675762 2.58919 -0.271298 3.90147 0.486347L23.0936 11.5669Z" fill="black"/>
    </svg>

)

export const ModalCloseIcon = () => (
    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M15 5L5 15" stroke="#9AA6AC" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
    <path d="M5 5L15 15" stroke="#9AA6AC" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
    </svg>

)

export const CancelIcon = () => (
    <svg width="28" height="28" viewBox="0 0 28 28" fill="#9AA6AC" xmlns="http://www.w3.org/2000/svg">
    <path d="M21 7L7 21" stroke="#9AA6AC" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
    <path d="M7 7L21 21" stroke="#9AA6AC" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
    </svg>

)

export const MenuBurgerIcon = () => (
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M4 6H20" stroke="#0071FF" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
    <path d="M4 12H20" stroke="#0071FF" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
    <path d="M4 18H20" stroke="#0071FF" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
    </svg>

)

export const MobileRightIcon = () => (
    <svg width="8" height="14" viewBox="0 0 8 14" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M7 13L1 7L7 1" stroke="#5B6871" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
    </svg>

)
export const MobileLeftIcon = () => (
    <svg width="8" height="14" viewBox="0 0 8 14" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M1 13L7 7L1 1" stroke="#5B6871" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
    </svg>
)

export const LeftSliderArrow = () => (
    <svg width="8" height="14" viewBox="0 0 8 14" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M7 13L1 7L7 1" stroke="#020A22" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
    </svg>

)
export const RightSliderArrow = () => (
    <svg width="8" height="14" viewBox="0 0 8 14" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M1 13L7 7L1 1" stroke="#020A22" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
    </svg>

)

import React from 'react'
import { RightSliderArrow, LeftSliderArrow, LeftArrowIcon, RightArrowIcon } from '../../svg.js'
import { Box } from '@mui/material'
import Image from 'next/image'

export function ArrowRight(props) {
  const { className, style, onClick, styles } = props
  return (
    <div className={`${className} ${styles || ''}`}>
      <Box
        // component={ArrowForwardIosIcon}
        sx={{
          ...style,
          background: '#fff',
          height: '48px',
          width: '48px',
          borderRadius: '50%',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          border: '1px solid #000',
          zIndex: '9999',
          marginLeft: '-30px',
          border: '1px solid #E8E9EB',
          '@media screen and (max-width: 767px)': {
            display: 'none'
          },
        }}
        onClick={onClick}
      >
        {/* <img src={<RightSliderArrow/>} alt=''/> */}
        <RightSliderArrow/>
      </Box>
    </div>
  )
}

export function ArrowLeft(props) {
  const { className, style, onClick, styles } = props
  return (
    <div className={`${className} ${styles || ''}`}>
      <Box
        sx={{
          ...style,
          background: '#fff',
          height: '48px',
          width: '48px',
          borderRadius: '50%',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          border: '1px solid #E8E9EB',
          zIndex: '9999',
          marginRight: '-11px',
          '@media screen and (max-width: 767px)': {
            display: 'none'
          },
        }}
        onClick={onClick}
      >
        {/* <img src={<RightSliderArrow/>} alt=''/> */}
        <LeftSliderArrow/>
      </Box>
    </div>
  )
}


// Banner Slider
export function BannerArrowRight(props) {
  const { className, style, onClick, styles } = props
  return (
    <div className={`${className} ${styles || ''}`}>
      <Box
        // component={ArrowForwardIosIcon}
        sx={{
          ...style,
          background: '#fff',
          height: '48px',
          width: '48px',
          borderRadius: '50%',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          border: '1px solid #000',
          zIndex: '9999',
          marginTop: '50px',
          marginLeft: '-240px',
          border: '1px solid #E8E9EB',
        }}
        onClick={onClick}
      >
        {/* <img src={<RightSliderArrow/>} alt=''/> */}
        <RightSliderArrow/>
      </Box>
    </div>
  )
}

export function BannerArrowLeft(props) {
  const { className, style, onClick, styles } = props
  return (
    <div className={`${className} ${styles || ''}`}>
      <Box
        sx={{
          ...style,
          background: '#fff',
          height: '48px',
          width: '48px',
          borderRadius: '50%',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          border: '1px solid #000',
          zIndex: '9999',
          marginLeft: '-20px',
          border: '1px solid #E8E9EB',
        }}
        onClick={onClick}
      >
        {/* <img src={<RightSliderArrow/>} alt=''/> */}
        <LeftSliderArrow/>
      </Box>
    </div>
  )
}

// Gallery Slider
export function GalleryArrowRight(props) {
  const { className, style, onClick, styles } = props
  return (
    <div className={`${className} ${styles || ''}`}>
      <Box
        // component={ArrowForwardIosIcon}
        sx={{
          ...style,
          background: '#fff',
          height: '48px',
          width: '48px',
          borderRadius: '50%',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          border: '1px solid #000',
          zIndex: 99,
          marginLeft: '-40px',
          border: 'none',
          background: 'rgba(255, 255, 255, 0.3)',
        }}
        onClick={onClick}
      >
        {/* <img src={<RightSliderArrow/>} alt=''/> */}
        <RightArrowIcon/>
      </Box>
    </div>
  )
}

export function GalleryArrowLeft(props) {
  const { className, style, onClick, styles } = props
  return (
    <div className={`${className} ${styles || ''}`}>
      <Box
        sx={{
          ...style,
          background: '#fff',
          height: '48px',
          width: '48px',
          borderRadius: '50%',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          border: '1px solid #E8E9EB',
          zIndex: 99,
          marginLeft: '50px',
          border: 'none',
          background: 'rgba(255, 255, 255, 0.3)',
        }}
        onClick={onClick}
      >
        {/* <img src={<RightSliderArrow/>} alt=''/> */}
        <LeftArrowIcon/>
      </Box>
    </div>
  )
}
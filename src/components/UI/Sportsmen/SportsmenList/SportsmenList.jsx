import React, { useState } from 'react';
import styles from '../sportsmen.module.scss'
import Image from 'next/image'
import { Dialog } from '@mui/material'
import { CancelIcon } from '../../../svg.js'
import { useIsMobile } from '../../../../utils/media'


function SportsmenList({ name, status, place, birthYear, img }) {
    const [infoModal, setInfoModal] = useState(false);
    const handleClick = () =>  setInfoModal(true);
    const handleClose = () =>  setInfoModal(false);
    
    const mobileTrue = useIsMobile(767)
    
    return (
        <div className={styles.sporstmen_list}>
          <div onClick={ mobileTrue[0] === true ? handleClick : null } className={styles.sportsmen_list_item}>
            <div className={styles.sportsmen_img}>
                <Image
                src={img}
                alt=''
                width={400}
                height={400}
                />
            </div>
            <div className={styles.sportsmen_info} >
                <h2>{name}</h2>
                <p className={styles.sportsmen_status}>{status}</p>
                <p className={styles.sportsmen_place}>Город: {place}</p>
                <p className={styles.sportsmen_birthDate}>Год рождения: {birthYear} </p>
                

            </div>
            </div>
                <Dialog open={infoModal}  onClose={handleClose} className='sportsmen_dialog'
                
                >
                    <div className={styles.mobile_sportsmenInfo}>
                        <div className={styles.mobile_sportsmenInfo_item}>
                            <button onClick={handleClose} className={styles.cancel_btn}><CancelIcon/></button>
                            <h2>Достижения</h2>
                            
                            <div className={styles.sportsmen_info_content}>
                                <p>
                                    <span>- 2008 год</span>
                                    Чемпион страны среди молодёжи
                                </p>
                                <p>
                                    <span>- 2008 год</span>
                                    Чемпион страны среди молодёжи
                                </p>
                                <p>
                                    <span>- 2008 год</span>
                                    Чемпион страны среди молодёжи
                                </p>
                                <p>
                                    <span>- 2008 год</span>
                                    Чемпион страны среди молодёжи
                                </p>
                                <p>
                                    <span>- 2008 год</span>
                                    Чемпион страны среди молодёжи
                                </p>
                                <p>
                                    <span>- 2008 год</span>
                                    Чемпион страны среди молодёжи
                                </p>
                            </div>
                        </div>
                    </div>
                </Dialog>    
        </div>
    );
}

export default SportsmenList;
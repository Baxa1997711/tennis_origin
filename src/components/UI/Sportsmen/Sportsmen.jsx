import React from 'react';
import styles from './sportsmen.module.scss'
import SportsmenList from './SportsmenList/SportsmenList';
import { sportsmen } from '../../../mock/sportsmen'
import { motion } from 'framer-motion'

const textAnimation = {
    hidden: {
      x: -100,
      opacity: 0
    },
    visible: custom =>({
      x: 0,
      opacity: 1,
      transition: { delay: custom * 0.2 },
    })
  }
  const blockAnimation = {
    hidden: {
      y: 100,
      opacity: 0
    },
    visible: custom =>({
      y: 0,
      opacity: 1,
      transition: { delay: custom * 0.2 },
    })
  }


function Sportsmen(props) {
    return (
        <motion.div initial='hidden' whileInView='visible' className={styles.sportsmen}>
            <div className={styles.sportsmen_content}>
                <motion.h4 variants={textAnimation} custom={1} className={styles.sportsmen_content_title}>Члены команды</motion.h4>
                
                <motion.div variants={blockAnimation} custom={2} className={styles.sportsmen_content_list}>
                  <div className={styles.sportsmen_reachInfo}>
                      <h3>Достижения</h3>
                    <div className={styles.sportsmen_content}>
                    <div className={styles.sportsmen_reachInf_item}>
                      <p>
                        <span>- 2008 год</span>
                        Чемпион страны среди молодёжи
                      </p>
                    </div>
                    </div>
                  </div>
                   { sportsmen?.map(item => (
                    <SportsmenList key={item?.id} name={item?.name} status={item?.status} place={item?.place} birthYear={item?.birthYear} img={item?.img}/> 
                   )) }
                </motion.div>
            </div>
        </motion.div>
    );
}

export default Sportsmen;
import styles from './Footer.module.scss'
import Link from 'next/link'
import { Container } from '@mui/material'
import useTranslation from 'next-translate/useTranslation'
import FooterLogo from '../../../../public/images/logo_text.svg'
import Image from 'next/image'
import { 
  InstagramIcon, 
  FacebookIcon,
  TelegramIcon
} from '../../svg.js';

export function Footer() {
  const { t } = useTranslation('common')
  
  
  return (
    <footer className={styles.footer}>
        <Container>
        <div className={styles.footer_content}>
          <div className={styles.footer_content_item}>
          <div className={styles.footer_logo}>
          <Image
          src={FooterLogo}
          alt=''
          />
          </div>
          <div className={styles.footer_links}>
            <Link href="/">
            <a className={styles.footer_link}>Главная</a>
            </Link>
            <Link href="/about">
            <a className={styles.footer_link}>О нас</a>
            </Link>
            <Link href="/news">
            <a className={styles.footer_link}>Новости</a>
            </Link>
            {/* <Link href="/gallery">
            <a className={styles.footer_link}>Галерея</a>
            </Link> */}
            <Link href="/video">
            <a className={styles.footer_link}>Видео</a>
            </Link>
            <Link href="/">
            <a className={styles.footer_link}>Контакты</a>
            </Link>
          </div>
          </div>
          <div className={styles.copy_rights}>
         <div className={styles.copy_rights_text}>
           ©UTTF 2022. Все права защищены
          </div>
          <div className={styles.socials}>
              <a href="">
                <InstagramIcon/>
              </a>
              <a href="">
              <FacebookIcon/>
              </a>
              <a href="">
                <TelegramIcon/>
              </a>
          </div> 
        </div> 
        </div>
        </Container>
    </footer>
  )
}

import Image from 'next/image'
import styles from './Main.module.scss'
import useTranslation from 'next-translate/useTranslation'
import { Button } from '@mui/material'
import { createPost, getPosts } from 'services'
import { useEffect, useState } from 'react'
import Container from '@mui/material/Container';
import Banner from '../Banner/Banner'
import NewsBlock from '../NewsBlock/NewsBlock'
import Partners from '../Partners/Partners'
import Competition from '../Competition/Competition'

export function Main() {
  const { t } = useTranslation('common')
  const [posts, setPosts] = useState([])

  useEffect(() => {
    getPosts({ limit: 10, page: 1 }).then((res) => {
      setPosts(res)
    })
  }, [])

  const addPost = () => {
    createPost(
      JSON.stringify({
        title: 'foo',
        body: 'bar',
        userId: 1,
      })
    ).then((res) => {
      console.log('create')
    })
  }

  return (
    <main className={styles.main}>
        <Banner/>
        <NewsBlock/>
        <Partners/> 
        <Competition/>
    </main>
  )
}

import React from 'react';
import styles from './newsBlockItem.module.scss';
import Image from 'next/image';
import { CalendarIcon2 } from '../../svg.js';
import Link from 'next/image';
import { motion } from 'framer-motion'

const textAnimation = {
    hidden: {
      x: -100,
      opacity: 0
    },
    visible: custom =>({
      x: 0,
      opacity: 1,
      transition: { delay: custom * 0.2 },
    })
  }
  const blockAnimation = {
    hidden: {
      y: 100,
      opacity: 0
    },
    visible: custom =>({
      y: 0,
      opacity: 1,
      transition: { delay: custom * 0.2 },
    })
  }


function NewsBlockItem({title, subtitle, date, img, handleBranch}) {

    return (
        <motion.div initial='hidden' whileInView='visible' viewport={{ amoun: 0.1, once: true}} className={styles.newsBlock_item} onClick={handleBranch}>
           <motion.div variants={blockAnimation} custom={1} className={styles.newsBlock_item_img}>
           <Image
            src={img}
            alt=''
            layout="fill"
            />
            </motion.div> 
            <div className={styles.newsBlock_context}>
            <div className={styles.newsBlock_date}>
              <CalendarIcon2 />
              <span>{date}</span>  
            </div>
            <h4 className={styles.newsBlock_item_title}>
            {title}
            </h4>
            <p className={styles.newsBlock_item_subtitle}>{subtitle}</p>
            </div>
        </motion.div>
    );
}

export default NewsBlockItem;
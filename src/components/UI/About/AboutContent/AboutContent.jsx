import React from 'react';
import styles from '../About.module.scss'
import Image from 'next/image'
import { about } from '../data.js'
import BannerImg from '../../../../../public/images/about_img1.png'

function AboutContent(props) {
    return (
        <div className={styles.about_context}>
            <div className={styles.about_banner}>
                <Image
                src={BannerImg}
                alt=''
                />
            </div>
            
            <div className={styles.about_content_text}>
                {about?.map(item => (
                    <div key={item?.id} className="">
                        <p key={item?.id} className={styles.about_content_subtitle}>
                        {item?.text1}
                        </p>
                        <p key={item?.id} className={styles.about_content_subtitle}>
                        {item?.text2}
                        </p>
                        <p key={item?.id} className={styles.about_content_subtitle}>
                        {item?.text3}
                        </p>
                        <p key={item?.id} className={styles.about_content_subtitle}>
                        {item?.text4}
                        </p>
                        <p key={item?.id} className={styles.about_content_subtitle}>
                        {item?.text5}
                        </p>
                        <div className={styles.about_img}>
                        <Image
                          src={item?.img1}
                          alt=''
                          width={768}
                          height={485}
                        />
                        </div>
                        <p key={item?.id} className={styles.about_content_subtitle}>
                        {item?.text6}
                        </p>
                        <p key={item?.id} className={styles.about_content_subtitle}>
                        {item?.text7}
                        </p>
                        <p key={item?.id} className={styles.about_content_subtitle}>
                        {item?.text8}
                        </p>
                        <p key={item?.id} className={styles.about_content_subtitle}>
                        {item?.text9}
                        </p>
                        <div className={styles.about_img}>
                        <Image
                          src={item?.img1}
                          alt=''
                          width={768}
                          height={485}
                        />
                        </div>
                        <p key={item?.id} className={styles.about_content_subtitle}>
                        {item?.text10}
                        </p>
                        <p key={item?.id} className={styles.about_content_subtitle}>
                        {item?.text11}
                        </p>
                        <p key={item?.id} className={styles.about_content_subtitle}>
                        {item?.text12}
                        </p>
                        <p key={item?.id} className={styles.about_content_subtitle}>
                        {item?.text13}
                        </p>
                        <p key={item?.id} className={styles.about_content_subtitle}>
                        {item?.text14}
                        </p>
                    </div>
                ))}
            </div>
        </div>
    );
}

export default AboutContent;
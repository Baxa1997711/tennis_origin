import { Container } from '@mui/material'
import useTranslation from 'next-translate/useTranslation'
import Image from 'next/image'
import styles from './About.module.scss'
import AboutContent from './AboutContent/AboutContent'
import  { motion } from 'framer-motion'

const textAnimation = {
  hidden: {
    x: -100,
    opacity: 0
  },
  visible: custom =>({
    x: 0,
    opacity: 1,
    transition: { delay: custom * 0.2 },
  })
}
const blockAnimation = {
  hidden: {
    y: 100,
    opacity: 0
  },
  visible: custom =>({
    y: 0,
    opacity: 1,
    transition: { delay: custom * 0.2 },
  })
}

export function About() {
  
  const { t } = useTranslation('about')
  return (
      <motion.div initial='hidden' whileInView='visible' viewport={{ amount: 0.1, once: true }} className={styles.about}>
        <Container>
        <div className={styles.about_content}>
        <motion.h1 variants={textAnimation} custom={1} className={styles.about_content_title}>О нас</motion.h1>
        
        <motion.div variants={blockAnimation} custom={2} className="">
          <AboutContent/>
        </motion.div>
        </div>
        </Container>
      </motion.div>
  )
}

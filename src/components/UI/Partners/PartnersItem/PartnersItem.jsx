import React from 'react';
import styles from '../sponsors.module.scss';
import Image from 'next/image';


function SponsorsItem({img}) {
    return (
        <div className={styles.sponsors_item}>
           <div className={styles.sponsors_img}>
            <Image
            src={img}
            alt=''
            width={'160px'}
            height={'70px'}
            />
            </div> 
        </div>
    );
}

export default SponsorsItem;
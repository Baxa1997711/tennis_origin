import React from 'react';
import styles from './sponsors.module.scss';
import PartnersItem from './PartnersItem/PartnersItem';
import { partners } from '../../../mock/partners'
import { Container } from '@mui/material';
import { motion } from 'framer-motion'

const textAnimation = {
    hidden: {
      x: -100,
      opacity: 0
    },
    visible: custom =>({
      x: 0,
      opacity: 1,
      transition: { delay: custom * 0.2 },
    })
  }
  const blockAnimation = {
    hidden: {
      y: 100,
      opacity: 0
    },
    visible: custom =>({
      y: 0,
      opacity: 1,
      transition: { delay: custom * 0.2 },
    })
  }


function Sponsors(props) {
    return (
        <motion.div initial='hidden' whileInView='visible' viewport={{ amount: 0.1 }} className={styles.sponsors}>
           <Container>
           <div className={styles.sponsors_content}>
            <motion.h4 variants={textAnimation} custom={1} className={styles.sponsors_content_title}>
                Партнёры   
            </motion.h4>
            <motion.div variants={blockAnimation} custom={2} className={styles.sponsor_content_item}>
            {partners?.map((item, index) => (
               <PartnersItem key={index} img={item?.img}/> 
            ))}
            </motion.div>
            </div> 
           </Container>
        </motion.div>
    );
}

export default Sponsors;
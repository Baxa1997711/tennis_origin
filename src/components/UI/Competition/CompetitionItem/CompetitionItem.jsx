import React, {useState} from 'react';
import styles from '../compet.module.scss';
import {  
    InfoArrowIcon
} from '../../../svg.js';
import { Dialog } from '@mui/material'
import { makeStyles } from '@mui/styles'
import { ModalCloseIcon } from '../../../svg.js'


const useStyles = makeStyles({
    modal: {
        borderRadius: '12px !important'
    }
})

function CompetitionItem({ title, subtitle, date }) {
    
    const classes = useStyles();
    
    const [openModal, setOpenModal] = useState(false);
    const handleClick= () => {
        setOpenModal(true)
      };
    
      const handleClose = () => {
        setOpenModal(false);
      };
      
    
    return (
        <>
        <div className={styles.competition_item} onClick={handleClick}>
           <div className={styles.competition_date}>{date}</div> 
           <h4 className={styles.competition_item_title}>{title}</h4>
            <p className={styles.competition_item_subtitle}>{subtitle}</p>
            <button className={styles.seeMore_button} onClick={handleClick}>
                Подробно 
                <InfoArrowIcon  className={styles.infoIcon}/>
            </button>
        </div>
        
        <Dialog
            onClose={handleClose} 
            open={openModal}
            className='compet_dialog'
            >
                <div className={styles.modal}>
                    <div className={styles.modal_content}>
                        <h3 className={styles.modal_content_title}>{title}</h3>
                        <p className={styles.modal_content_date}>{date}</p>
                        <p className={styles.modal_content_subtitle}>{subtitle}</p>
                    </div>
                    <button className={styles.modal_closeBtn} onClick={handleClose}>
                       <ModalCloseIcon/> 
                    </button>
                </div>
         </Dialog>
        
        </>
    );
}

export default CompetitionItem;
import { Container } from '@mui/material';
import { makeStyles } from '@mui/styles'
import React, {useState} from 'react';
import styles from './compet.module.scss';
import CompetitionItem from './CompetitionItem/CompetitionItem';
import { competition } from '../../../mock/competition'
import { motion } from 'framer-motion'
import Slider from 'react-slick'


const textAnimation = {
    hidden: {
      y: -100,
      opacity: 0
    },
    visible: custom =>({
      y: 0,
      opacity: 1,
      transition: { delay: custom * 0.2},
    })
  }
  const blockAnimation = {
    hidden: {
      y: 100,
      opacity: 0
    },
    visible: custom =>({
      y: 0,
      opacity: 1,
      transition: { delay: custom * 0.2 },
    })
  }
  
  const useStyles = makeStyles({
    slider: {
      minHeight: '220px'
    }
  })

function Competition(props) {
  
  const classes = useStyles();
    
    const [openModal, setOpenModal] = useState(false);
    const [apiData, setApiData] = useState([])
    
    const handleOpen = () => setOpenModal(true);
    const handleClose = () => setOpenModal(false);
    
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      dotsClass: `slick-dots ${styles.dots}`
    }
    
    
    return (
        <motion.div initial='hidden' whileInView='visible' viewport={{ amount: 0.1, once: true }} className={styles.competition} id='competition'>
            <Container>
            <div className={styles.competition_content}>
                <motion.h4 custom={1} variants={textAnimation} className={styles.competition_content_title}>
                    Соревновании
                </motion.h4>
                
               <motion.div initial='hidden' whileInView='visible' viewport={{ amount: 0.2, once: true}} className="">
                <motion.div custom={2} variants={blockAnimation} className={styles.competition_content_item}>
                    {competition && competition?.map(item => (
                        <CompetitionItem  key={item?.id} title={item?.title} date={item?.date} subtitle={item?.subtitle}/>                    
                    ))}
                </motion.div>
                
                <motion.div custom={2} variants={blockAnimation} className={styles.mobileCompetition_content_item}>
                    <Slider {...settings} className='compet-slider'>
                    {competition && competition?.map(item => (
                        <CompetitionItem  key={item?.id} title={item?.title} date={item?.date} subtitle={item?.subtitle}/>                    
                    ))}
                    </Slider>
                </motion.div>
               </motion.div>
            </div>
            </Container>
        </motion.div>
    );
}

export default Competition;
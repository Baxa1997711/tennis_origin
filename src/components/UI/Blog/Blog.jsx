import { Container } from '@mui/material'
import Link from 'next/link'
import styles from './Blog.module.scss'
import useTranslation from 'next-translate/useTranslation'

export function BlogList() {
  const { t } = useTranslation('common')
  return (
    <div className={styles.bloglist} id='banner'>
      <Container className='container'>
        <h2>Blog</h2>
    </Container>
    </div>
  )
}

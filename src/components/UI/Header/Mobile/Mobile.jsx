import React, {useState} from 'react';
import styles from '../Header.module.scss'
import { Drawer, Container, AppBar, Box, Button } from '@mui/material'
import Link from 'next/link'
import Image from 'next/image'
import LogoIcon from '../../../../../public/images/logo_text.svg'
import { MenuBurgerIcon } from '../../../svg.js'


function Mobile(props) {
  const [openDrawer, setOpenDrawer] = useState(false)
  
  const handleClick = () => setOpenDrawer(true)
  const handleClose = () => setOpenDrawer(false)
  
    return (
        <>
             <div className={styles.mobile_header_content}>
             <Container>
               <Box>
                   <div className={styles.mobile_header_item}>
                       <div className={styles.mobile_header_logo}>
                         <Link href="/">
                           <a >
                               <Image
                               src={LogoIcon}
                               alt=''
                               width={88}
                               height={30.38}
                               />
                           </a>
                         </Link>  
                       </div>
                       <div className="">
                         {!openDrawer && 
                         <Button onClick={handleClick} className={styles.menuBurger_btn}><MenuBurgerIcon/></Button>
                         }
                       </div>
                   </div>
               </Box>
             </Container>
           </div>
        
        <Drawer
        open={openDrawer}
        anchor='left'
        onClose={handleClose}
        >
          <div className={styles.mobile_header_links}>
          <Link href="/">
            <a className={styles.mobile_logo} onClick={handleClose}>
                <Image
                src={LogoIcon}
                alt=''
                width={88}
                height={30.38}
                />
            </a>
          </Link> 
          <div className={styles.mobile_header_linkItem}>
            <Link href="/about">
              <a className={styles.mobile_header_link} onClick={handleClose}>
                О нас
              </a>
            </Link>
            <Link href="/management">
              <a className={styles.mobile_header_link} onClick={handleClose}>
                Управление
              </a>
            </Link>
            <Link href="/coaches">
              <a className={styles.mobile_header_link} onClick={handleClose}>
                Тренеры
              </a>
            </Link>
            <Link href="/sportsmen">
              <a className={styles.mobile_header_link} onClick={handleClose}>
                Члены команды
              </a>
            </Link>
            <Link href="/news">
              <a className={styles.mobile_header_link} onClick={handleClose}>
                Новости
              </a>
            </Link>
            <Link href="/#competition">
              <a className={styles.mobile_header_link} onClick={handleClose}>
                Соревнования
              </a>
            </Link>
            <Link href="/gallery">
              <a className={styles.mobile_header_link} onClick={handleClose}>
                Галерея
              </a>
            </Link>
            <Link href="/video">
              <a className={styles.mobile_header_link} onClick={handleClose}>
               Видео
              </a>
            </Link>
          </div>
          </div>
        </Drawer>
        </>
    );
}

export default Mobile;
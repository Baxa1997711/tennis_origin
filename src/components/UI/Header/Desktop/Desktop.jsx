import React, { useState } from 'react';
import Link from 'next/link'
import Image from 'next/image'
import styles from '../Header.module.scss'
import { useRouter } from 'next/router'
import { Container, Button, Menu } from '@mui/material'
import { 
    CalendarIcon,
    DropDownIcon,
    MenuBurgerIcon
  
  } from '../../../svg.js';
  import LogoIcon from '../../../../../public/images/logo_text.svg'
  import { makeStyles } from '@mui/styles'
  
  const useStyles = makeStyles({
    popover: {
      top: '28px !important',
        }
    })

function Desktop(props) {
    const router = useRouter()
    const classes = useStyles();
    
    const [menuValue, setMenuValue] = useState(null);
    const open = Boolean(menuValue);
    const handleClick = (e) => {
    setMenuValue(e.currentTarget);
    };
    const handleClose = () => {
    setMenuValue(null);
    };
    return (
        <header className={styles.header} id='header'>
        <Container >
        <div className={styles.header_content}>
        <div className={styles.header_item}>
          <div className={styles.header_item_logo}>
          <Link href="/">
          <a className={styles.header_logo} onMouseOver={handleClose}>
              <Image
              src={LogoIcon}
              alt=''
              width={88}
              height={30.38}
              />
          </a>
          </Link>
          {/* <div className={styles.calendar_tab}>
            <CalendarIcon/>
            <span className={styles.calendar_title}>Июнь 10, 2022</span>
          </div> */}
          </div>
        </div>
        <div className={styles.navbar}>
          <div className={styles.navbar_items}>
            <Link href="">
              <a className={router.pathname === '/about' ? styles.navbar_activeLink : router.pathname === '/about' ? 
              styles.navbar_activeLink : router.pathname === '/management' ? styles.navbar_activeLink : router.pathname === '/sportsmen' ? styles.navbar_activeLink  : router.pathname === '/coaches' ? styles.navbar_activeLink : styles.navbar_links} 
              onMouseOver={handleClick} onClick={handleClick}
              >
                UTTF 
                <DropDownIcon />
              </a>
            </Link>
            
            <Link href="/news">
            <a onMouseOver={handleClose}
              className={router.pathname === '/news' ? styles.navbar_activeLink : styles.navbar_links}>
              Новости
            </a>
            </Link>
            <Link href="/#competition">
            <a onMouseOver={handleClose}
              className={router.pathname === '/#competition' ? styles.navbar_activeLink : styles.navbar_links}>
              Соревнования
            </a>
            </Link>
            <Link href="/gallery">
            <a onMouseOver={handleClose}
              className={router.pathname === '/gallery' ? styles.navbar_activeLink : styles.navbar_links}>
              Галерея
            </a>
            </Link>
            <Link href="/video">
            <a onMouseOver={handleClose}
              className={router.pathname === '/video' ? styles.navbar_activeLink : styles.navbar_links}>
              Видео
            </a>
            </Link>
            
            
              <Menu
              id="basic-menu"
              anchorEl={menuValue}
              open={open}
              onClose={handleClose}
              MenuListProps={{
                'aria-labelledby': 'basic-button',
              }}
              className='header_popover'
              onMouseLeave={handleClose}
              >
              <div className={styles.dropDown_navbar} id='dropdown' onClick={handleClose} onMouseLeave={handleClose}>
                <Link href="/about">
                <a className={styles.dropDown_link} onClick={handleClose}>О нас</a>
                </Link>
                <Link href="/management">
                <a className={styles.dropDown_link} onClick={handleClose}>Управление</a>
                </Link>
                <Link href="/coaches">
                <a className={styles.dropDown_link} onClick={handleClose}>Тренеры</a>
                </Link>
                <Link href="/sportsmen">
                <a className={styles.dropDown_link} onClick={handleClose}>Члены команды</a>
                </Link>
              </div>
              </Menu>
          </div>
        </div>
        </div>
        </Container>
      </header>
    );
}

export default Desktop;
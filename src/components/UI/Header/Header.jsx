import { Container } from '@mui/material'
import useTranslation from 'next-translate/useTranslation'
import { useState } from 'react'
import { useRouter } from 'next/router'
import Desktop from './Desktop/Desktop'
import styles from './Header.module.scss'
import Mobile from './Mobile/Mobile'



export function Header() {
  const router = useRouter()
  const { t } = useTranslation('common')
  const langs = [
    {
      key: 'ru',
      label: 'ru',
    },
    {
      key: 'uz',
      label: 'uz',
    },
    {
      key: 'en',
      label: 'en',
    },
  ]    
  
  return (
   <>
      <div className={styles.desktop_header}>
        <Desktop/>
      </div>
      
      <div className={styles.mobile_header}>
        <Mobile/>
      </div>
   </>
  )
}

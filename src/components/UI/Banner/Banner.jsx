import React, { useRef } from 'react';
import Image from 'next/image';
import styles from './banner.module.scss';
import Slider from 'react-slick';
import { makeStyles } from '@mui/styles';
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import { Button, Container } from '@mui/material'
import { data } from './data'
import { 
    LeftArrowIcon,
    RightArrowIcon,
    MobileRightIcon,
    MobileLeftIcon

} from '../../svg.js';
// import { BannerArrowRight, BannerArrowLeft } from '../SlickCarouselArrows/Arrows'
import { motion } from 'framer-motion'


const textAnimation = {
  hidden: {
    x: -100,
    opacity: 0
  },
  visible: custom =>({
    x: 0,
    opacity: 1,
    transition: { delay: custom * 0.2 },
  })
}


function Banner(props) {
    const settings = {
        fade: true,
        infinite: true,
        speed: 800,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false, 
    }
    const slider = useRef();
    
    const next = () => {
      slider.current.slickNext();
    }
    const previous = () => {
      slider.current.slickPrev();
    }
    
    return (
       <div className={styles.banner_wrapper}>
         <Slider ref={slider} {...settings} className={styles.banner_slider}>
            {data?.map(item => (
              <div key={item?.id} className={styles.banner}>
              <div className={styles.banner_img}>
                <Image
                src={item?.image}
                alt=''
                width={1440}
                height={670}
                // objectFit='cover'
                />
              </div>
              <div className={styles.banner_info}>
                <Container {...settings}>
                  <div className={styles.banner_info_item}>
                    <h2>{item?.title}</h2>
                    <p>{item?.subtitle}</p>
                    <div className={styles.banner_control_panel}>
                      <Button className={styles.banner_content_btn}>Читать подробно</Button>
                      <div className={styles.banner_control_arrows}>
                        <button onClick={previous} className={styles.btn}><RightArrowIcon/></button>
                        <button onClick={next} className={styles.btn}><LeftArrowIcon/></button>
                      </div>
                    </div>
                  </div>
                </Container>
              </div>
              
              <div className={styles.mobile_info}>
                  <p className={styles.mobile_info_text}>{item?.subtitle}</p>
                  <div className={styles.mobileBanner_control_panel}>
                    <Button variant="contained" className={styles.mobileBanner_readMore}>Читать подробно</Button>
                    <div className={styles.mobile_arrow_buttons}>
                        <button onClick={next} className={styles.mobile_arrow_left} ><MobileRightIcon/></button>
                        <button onClick={previous} className={styles.mobile_arrow_right} ><MobileLeftIcon/></button>
                    </div>
                </div>
              </div>
            </div>
            ))}
         </Slider>
       </div> 
    );
}

export default Banner;
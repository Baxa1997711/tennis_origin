import { Container } from '@mui/material';
import React from 'react';
import styles from './newsBlock.module.scss';
import NewsBlockItem from '../NewsBlockItem/NewsBlockItem';
import { news } from '../../../mock/news';
import { useRouter } from 'next/router';
import { motion } from 'framer-motion'

const textAnimation = {
  hidden: {
    y: 100,
    opacity: 0
  },
  visible: custom =>({
    y: 0,
    opacity: 1,
    transition: { delay: custom * 0.2 },
  })
}
const blockAnimation = {
  hidden: {
    y: 100,
    opacity: 0
  },
  visible: custom =>({
    y: 0,
    opacity: 1,
    transition: { delay: custom * 0.2 },
  })
}



function NewsBlock(props) {
  const router = useRouter();
  const handleBranch = (id) => {
    router.push(`/news/${id}`)
  }
  
    return (
        <motion.div initial='hidden' whileInView='visible' viewport={{ amount: 0.1, once: true }}  className={styles.newsBlock}>
           <Container >
           <div className={styles.newsBlock_content}>
              <motion.h2 custom={1} variants={textAnimation} className={styles.newsBlock_content_title}>Последние новости</motion.h2>
              <motion.div custom={2} variants={blockAnimation} className={styles.newsBlock_content_item}>
              { news?.map((item, index) => (
                <NewsBlockItem 
                key={index}
                title={item?.title}
                handleClick={() => handleBranch(index)}
                subtitle={item?.subtitle}
                date={item?.date}
                img={item?.img}
                />  
              )) }
              </motion.div>
           </div>
           </Container>
        </motion.div>
    );
}

export default NewsBlock;
import SEO from 'seo'
import NewsPage from '../../components/pages/News/NewsPage';

export default function News({}) {
  return (
    <>
      <SEO />
      <NewsPage />
    </>
  )
}
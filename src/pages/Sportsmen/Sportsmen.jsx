import React from 'react';
import styles from './sportsmen.module.scss'
import SportsmenList from '../../components/UI/Sportsmen//SportsmenList/SportsmenList';
import { sportsmen } from '../../mock/sportsmen'

function Sportsmen(props) {
    return (
        <div className={styles.sportsmen}>
            <div className={styles.sportsmen_content}>
                <h4 className={styles.sportsmen_content_title}>Члены команды</h4>
                
                <div className={styles.sportsmen_content_list}>
                   { sportsmen?.map(item => (
                    <SportsmenList key={item?.id} name={item?.name} status={item?.status} place={item?.place} birthYear={item?.birthYear} img={item?.img}/> 
                   )) }
                </div>
            </div>
        </div>
    );
}

export default Sportsmen;